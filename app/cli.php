<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 12.8.2014
 * Time: 12:50
 */

use Phalcon\DI\FactoryDefault\CLI as CliDI;
use Phalcon\CLI\Console as ConsoleApp;

define('VERSION', '1.0.0');

$di = new CliDI();

defined('APPLICATION_PATH')|| define('APPLICATION_PATH', realpath(dirname(__FILE__)));
if(is_readable(APPLICATION_PATH . '/config/config.ini')) {
	$config = new \Phalcon\Config\Adapter\Ini(APPLICATION_PATH . '/config/config.ini');
	$di->set('config', $config);
}
else {
	echo "Unable to read configuration file!\n";
	exit(255);
}

include (APPLICATION_PATH . '/config/loader.php');
include (APPLICATION_PATH . '/config/services.php');

$console = new ConsoleApp();
$console->setDI($di);

$arguments = array();
foreach($argv as $k => $arg) {
	if($k == 1) {
		$arguments['task'] = $arg;
	} elseif($k == 2) {
		$arguments['action'] = $arg;
	} elseif($k >= 3) {
		$arguments['params'][] = $arg;
	}
}

define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

try {
	$console->handle($arguments);
}
catch (\Phalcon\Exception $e) {
	echo $e->getMessage();
	exit(255);
}