<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this meta tag, Half Life 3 will never be released. -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Infos2 Newsletter</title>

<style type="text/css">
/* -------------------------------------
		GLOBAL
------------------------------------- */
* {
	margin:0;
	padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

img {
	max-width: 100%;
}
.collapse {
	margin:0;
	padding:0;
}
body {
	-webkit-font-smoothing:antialiased;
	-webkit-text-size-adjust:none;
	width: 100%!important;
	height: 100%;
}


/* -------------------------------------
		ELEMENTS
------------------------------------- */
a { color: #2BA6CB;}

.btn {
	text-decoration:none;
	color: #FFF;
	background-color: #666;
	padding:10px 16px;
	font-weight:bold;
	margin-right:10px;
	text-align:center;
	cursor:pointer;
	display: inline-block;
}

p.callout {
	padding:15px;
	background-color:#ECF8FF;
	margin-bottom: 15px;
}
.callout a {
	font-weight:bold;
	color: #2BA6CB;
}

table.social {
	/* 	padding:15px; */
	background-color: #ebebeb;

}
.social .soc-btn {
	padding: 3px 7px;
	font-size:12px;
	margin-bottom:10px;
	text-decoration:none;
	color: #FFF;font-weight:bold;
	display:block;
	text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.tw { background-color: #1daced!important; }
a.gp { background-color: #DB4A39!important; }
a.ms { background-color: #000!important; }

.sidebar .soc-btn {
	display:block;
	width:100%;
}

/* -------------------------------------
		HEADER
------------------------------------- */
table.head-wrap {
	width: 100%;
	background: #333333;
}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}


/* -------------------------------------
		BODY
------------------------------------- */
table.body-wrap { width: 100%;}


/* -------------------------------------
		FOOTER
------------------------------------- */
table.footer-wrap { width: 100%;	clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
	font-size:10px;
	font-weight: bold;

}


/* -------------------------------------
		TYPOGRAPHY
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
	font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 37px;}
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

.collapse { margin:0!important;}

p, ul {
	margin-bottom: 10px;
	font-weight: normal;
	font-size:14px;
	line-height:1.6;
}
p.lead { font-size:17px; }
p.last { margin-bottom:0px;}

ul li {
	margin-left:5px;
	list-style-position: inside;
}

/* -------------------------------------
		SIDEBAR
------------------------------------- */
ul.sidebar {
	background:#ebebeb;
	display:block;
	list-style-type: none;
}
ul.sidebar li { display: block; margin:0;}
ul.sidebar li a {
	text-decoration:none;
	color: #666;
	padding:10px 16px;
	/* 	font-weight:bold; */
	margin-right:10px;
	/* 	text-align:center; */
	cursor:pointer;
	border-bottom: 1px solid #777777;
	border-top: 1px solid #FFFFFF;
	display:block;
	margin:0;
}
ul.sidebar li a.last { border-bottom-width:0px;}
ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



/* ---------------------------------------------------
		RESPONSIVENESS
		Nuke it from orbit. It's the only way to be sure.
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display:block!important;
	max-width:600px!important;
	margin:0 auto!important; /* makes it centered */
	clear:both!important;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	padding:15px;
	max-width:600px;
	margin:0 auto;
	display:block;
}

/* Let's make sure tables in the content area are 100% wide */
.content table { width: 100%; }


/* Odds and ends */
.column {
	width: 300px;
	float:left;
}
.column tr td { padding: 15px; }
.column-wrap {
	padding:0!important;
	margin:0 auto;
	max-width:600px!important;
}
.column table { width:100%;}
.social .column {
	width: 280px;
	min-width: 279px;
	float:left;
}

/* Be sure to place a .clear element after each set of columns, just to be safe */
.clear { display: block; clear: both; }


/* -------------------------------------------
		PHONE
		For clients that support media queries.
		Nothing fancy.
-------------------------------------------- */
@media only screen and (max-width: 600px) {

	a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

	div[class="column"] { width: auto!important; float:none!important;}

	table.social div[class="column"] {
		width:auto!important;
	}

}

.suploTable {
	width: 100%;
	margin: 0 auto;
	font-size: .9em;
	line-height: 1.4em;
}

.suploTable th {
	text-align: left;
	border-bottom: 1px solid #000000;
}

.suploTable th, .suploTable td{
	padding: .6em;
}

</style>

</head>

<body>

<!-- HEADER -->
<table class="head-wrap">
	<tr>
		<td class="header container" >
			<div class="content">
				<table>
					<tr>
						<td><img src="{{ applicationURI }}public/images/logo.png" alt="Infos2" style="height: 35px"/></td>
						<td align="right"><h6 class="collapse" style="color: white;">{{ "now"|date("j. n. Y", 'Europe/Bratislava') }}</h6></td>
					</tr>
				</table>
			</div>
		</td>
		<td></td>
	</tr>
</table><!-- /HEADER -->

<!-- BODY -->
<table class="body-wrap">
	<!-- Announcements -->
	{% if newsletter.nws_announcements != "0" and announcements is not null %}
	<tr>
		<td class="container" bgcolor="#FFFFFF">
			<div class="content">
				<table>
					{% for announcement in announcements %}
						<tr>
							<td>
								<h3 class="collapse">{{ announcement.ann_title|raw }}</h3>
								{{ announcement.ann_text|raw }}
							</td>
						</tr>
					{% endfor %}
				</table>
			</div><!-- /content -->
		</td>
	</tr>
	<!-- Separator -->
	<tr>
		<td class="container" bgcolor="#FFFFFF">
			<div class="content">
				<table>
					<tr>
						<td>
							<hr style="margin: 10px 0;"/>
						</td>
					</tr>
				</table>
			</div><!-- /content -->
		</td>
	</tr>
	{% endif %}

	<!-- Events -->
	{% if newsletter.nws_events != "0" and events is not null %}
		<tr>
			<td class="container" bgcolor="#FFFFFF">
				<div class="content">
					<table>
						{% for record in events %}
							<tr>
								<td>
									<h3 class="collapse">{{ record.evt_title }}</h3>
									<p>
										<strong>Typ: </strong> {{ record.evy_label }}
										<br />
										<strong>Kedy: </strong> {{ record.evt_date|date("j. n. Y") }} {{ record.evt_startTime|date("H:i") }} - {{ record.evt_endTime|date("H:i") }}
										<br />
										<strong>Kde: </strong> {{ record.evt_place }}
									</p>
									{% autoescape %}
										{% if record.evt_program %}
											<strong>Program:</strong> <br />
											{{ record.evt_program|raw }}
										{% endif %}
										{% if record.evt_description %}
											<strong>Popis:</strong> <br />
											{{ record.evt_description|raw }}
										{% endif %}
									{% endautoescape %}
								</td>
							</tr>
						{% endfor %}
					</table>
				</div><!-- /content -->
			</td>
		</tr>
		<!-- Separator -->
		<tr>
			<td class="container" bgcolor="#FFFFFF">
				<div class="content">
					<table>
						<tr>
							<td>
								<hr style="margin: 10px 0;"/>
							</td>
						</tr>
					</table>
				</div><!-- /content -->
			</td>
		</tr>
	{% endif %}

	{% if suplo is not null %}
		<tr>
			<td class="container" bgcolor="#FFFFFF">
				<div class="content">
					<table>
						<tr>
							<td>
								<h3>Supovanie</h3>
								<table class="suploTable">
									<thead>
									<tr>
										<th>Hodina</th>
										<th>Vyučujúci</th>
										<th>Trieda</th>
										<th>Predmet</th>
										<th>Učebňa</th>
										<th>Zastupujúci</th>
										<th>Poznámka</th>
									</tr>
									</thead>
									<tbody>
									{% for day in suplo %}
										<tr>
											<th colspan="7" style="text-align: center;">{{ day.date|date("j. n. Y") }}</th>
										</tr>
										{% for record in day.suplo %}
											<tr>
												<td>{{ record.tmt_label }}</td>
												<td>{{ record.missingName }}</td>
												<td>{{ record.sup_classes }}</td>
												<td>{{ record.sup_subject }}</td>
												<td>{{ record.sup_classroom }}</td>
												<td>{{ record.usr_firstName }} {{ record.usr_lastName }}</td>
												<td>{{ record.sup_note }}</td>
											</tr>
										{% endfor %}
									{% endfor %}
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</div><!-- /content -->
			</td>
		</tr>
	{% endif %}

</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap">
	<tr>
		<td></td>
		<td class="container">
			<!-- content -->
			<div class="content">
				<table>
					<tr>
						<td align="center">
							<p>
								<a href="{{ config.aplication.infosUri }}">Infos2 - GVPT</a>
							</p>
						</td>
					</tr>
				</table>
			</div><!-- /content -->
		</td>
		<td></td>
	</tr>
</table><!-- /FOOTER -->

</body>
</html>