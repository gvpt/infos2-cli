<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 12.8.2014
 * Time: 13:25
 */

use Phalcon\CLI\Task;

//TODO: Logging

class GoogleTask extends Task {

	private $tokens = [];

	private function verifyToken($id_token) {
		$url = 'https://www.googleapis.com/oauth2/v1/tokeninfo?id_token=' . $id_token;

		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		$result = curl_exec($ch);

		curl_close($ch);

		return json_decode($result);
	}

	private function refreshToken() {
		echo "Refreshing authorization token \n";
		$url = 'https://accounts.google.com/o/oauth2/token';
		$fields = array(
			'refresh_token' => $this->tokens['refresh'],
			'client_id' => $this->config->google->clientId,
			'client_secret' => $this->config->google->clientSecret,
			'grant_type' => 'refresh_token'

		);
		$fields_string = http_build_query($fields);

		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		$result = curl_exec($ch);

		curl_close($ch);

		$result = json_decode($result);

		if (!isset($result->error)) {
			$this->tokens['access'] = $result->access_token;
			$this->tokens['expires'] = $result->expires_in;
			$result = $this->verifyToken($result->id_token);
			if (!isset($result->error)) {
				$this->tokens['issued'] = $result->issued_at;
				$this->tokens['expires'] = $this->tokens['issued'] + $this->tokens['expires'];
				$sql = "UPDATE cliUser SET cli_accessToken = :accessToken, cli_expires = :expires, cli_issued = :issued WHERE cli_email = :email";
				$data = array(
					'accessToken' => $this->tokens['access'],
					'expires' => date("Y-m-d H:i:s", $this->tokens['expires']),
					'issued' => date("Y-m-d H:i:s", $this->tokens['issued']),
					'email' => $result->email
				);
				try {
					$result = $this->db->execute($sql, $data);
				}
				catch (Exception $e) {
					echo "SQL error:" . $e->getMessage() . " \n";
					return false;
				}
				if ($result) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				echo "Google auth error " . $result->error . ":" . $result->error_description . " \n";
				return false;
			}
		}
		else {
			echo "Google auth error " . $result->error . ":" . $result->error_description . " \n";
			return false;
		}
	}

	private function getAccessToken() {
		if($this->authorize()) {
			if (time() > $this->tokens['expires']) {
				echo "Access token expired \n";
				if (!$this->refreshToken()) {
					return false;
				}
			}
			return $this->tokens['access'];
		}
		else {
			return false;
		}
	}

	private function authorize() {
		$email = $this->config->google->user;
		$sql = "SELECT * FROM cliUser WHERE cli_email = '$email'";
		try {
			$result = $this->db->fetchOne($sql, Phalcon\Db::FETCH_OBJ);
		}
		catch (Exception $e) {
			echo "SQL error:" . $e->getMessage() . " \n";
			return false;
		}

		if ($result) {
			$this->tokens['access'] = $result->cli_accessToken;
			$this->tokens['refresh'] = $result->cli_refreshToken;
			$this->tokens['expires'] = strtotime($result->cli_expires);
			$this->tokens['issued'] = strtotime($result->cli_issued);
			return true;
		}
		else {
			echo "Authorization of $email has failed! \n";
			return false;
		}
	}

	private function createCalendarEvent($data) {
		echo "Creating event: ";
		$calendarId = $data['calendar'];
		$url = "https://www.googleapis.com/calendar/v3/calendars/$calendarId/events?fields=id";

		//Start time
		$startTime = new DateTime($data['date']);
		$time = explode(':', $data['start']);
		$startTime->setTime($time[0], $time[1], $time[2]);

		//End time
		$endTime = new DateTime($data['date']);
		$time = explode(':', $data['end']);
		$endTime->setTime($time[0], $time[1], $time[2]);

		$fields = array(
			'start'			=> array(
				'dateTime' 	=> $startTime->format(DateTime::RFC3339),
				'timezone'	=> 'Europe/Bratislava'
			),
			'end'			=> array(
				'dateTime'	=> $endTime->format(DateTime::RFC3339),
				'timezone'	=> 'Europe/Bratislava'
			),
			'summary'		=> $data['summary'],
			'description'	=> $data['desc'],
			'location'		=> $data['location']
		);
		$fields_string = json_encode($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($fields_string),
			'Authorization: Bearer ' . $this->getAccessToken()
		));

		$result = curl_exec($ch);

		curl_close($ch);

		$result = json_decode($result);

		if (!isset($result->id)) {
			echo $result->error->message;
			return false;
		}
		else {
			echo $result->id;
			return $result->id;
		}

	}

	public function createSuploAction() {
		echo date("Y-m-d H:i:s") . "\n";
		$sql = "SELECT id_suplo, sup_date, sup_classes, sup_classroom, sup_subject, usr_calendarSuplo, tmt_label, tmt_endTime, tmt_startTime, sup_nick, usr_google FROM suplo LEFT JOIN user ON user.id_user = suplo.id_user LEFT JOIN timetable ON timetable.id_timetable = suplo.id_timetable WHERE sup_eventId IS NULL";
		try {
			$result = $this->db->fetchAll($sql, Phalcon\Db::FETCH_OBJ);
		}
		catch(Exception $e) {
			echo " SQL error:" . $e->getMessage() . " \n";
			exit();
		}
		if ($result) {
			foreach ($result as $record) {
				if ($record->usr_google) {
					$data = array(
						'date'		=> $record->sup_date,
						'start'		=> $record->tmt_startTime,
						'end'		=> $record->tmt_endTime,
						'location'	=> $record->sup_classroom,
						'calendar'	=> $record->usr_calendarSuplo,
						'summary'	=> 'Suplovanie ' . $record->tmt_label . ' - ' . $record->sup_subject,
						'desc'		=> $record->sup_classes . ' namiesto ' . $record->sup_nick
					);
					$eventId = $this->createCalendarEvent($data);
					$suploId = $record->id_suplo;
					$sql = "UPDATE suplo SET sup_eventId = '$eventId' WHERE id_suplo = $suploId";
					try {
						$success = $this->db->execute($sql);
					}
					catch (Exception $e) {
						echo " SQL error:" . $e->getMessage() . " \n";
						exit();
					}
					if ($success) {
						echo " OK \n";
					}
					else {
						echo " ERR \n";
					}
				}
			}
		}
		else {
			echo "Nothing to do \n";
		}
		echo "Done\n";
	}

	public function createEventsAction() {
		echo date("Y-m-d H:i:s") . "\n";
		$sql = "SELECT evt_title, evt_endTime, evt_startTime, evt_date, evt_description, evt_place, evy_label, id_event FROM event LEFT JOIN eventType ON eventType.id_eventType = event.id_eventType WHERE evt_google = FALSE";
		try {
			$events = $this->db->fetchAll($sql, Phalcon\Db::FETCH_OBJ);
		}
		catch (Exception $e) {
			echo " SQL error:" . $e->getMessage() . " \n";
			exit();
		}
		if ($events) {
			foreach ($events as $event) {
				$sql = "SELECT usr_calendarSuplo, id_user, usr_email FROM user WHERE usr_google = 1";
				try {
					$users = $this->db->fetchAll($sql, Phalcon\Db::FETCH_OBJ);
				}
				catch (Exception $e) {
					echo " SQL error:" . $e->getMessage() . " \n";
					exit();
				}
				$eventId = $event->id_event;
				echo "Event: $eventId \n\n";
				foreach ($users as $user) {
					echo "\t" . $user->usr_email . "\n\t";
					$data = array(
						'date'		=> $event->evt_date,
						'start'		=> $event->evt_startTime,
						'end'		=> $event->evt_endTime,
						'location'	=> $event->evt_place,
						'calendar'	=> $user->usr_calendarSuplo,
						'summary'	=> $event->evt_title,
						'desc'		=> $event->evy_label
					);
					$googleEventId = $this->createCalendarEvent($data);
					$userId = $user->id_user;
					$sql = "INSERT INTO eventGoogle (id_user, id_event, id_eventGoogle) VALUES ('$userId', $eventId, '$googleEventId')";
					try {
						$success = $this->db->execute($sql);
					}
					catch (Exception $e) {
						echo " SQL error:" . $e->getMessage() . " \n";
						exit();
					}
					if ($success) {
						echo " OK \n";
					}
					else {
						echo " ERR \n";
					}
				}
				echo "\nUpdating event table\t";
				$sql = "UPDATE event SET evt_google = TRUE WHERE id_event = $eventId";
				try {
					$success = $this->db->execute($sql);
				}
				catch (Exception $e) {
					echo "SQL error:" . $e->getMessage() . " \n";
					exit();
				}
				if ($success) {
					echo "OK \n";
				}
				else {
					echo "ERR \n";
				}
			}
		}
		echo "Done\n";
	}

	public function testAuthServerAction() {
		$url = 'localhost:4567/verify';
		$token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQzY2YyY2NjZWY2NTRmMmJhNTc0OTUwY2YxZjQ1ZTJlZTBkOThlYzYifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiaWQiOiIxMTUwNTgwODg3Mjg3NzYyMDY4NjAiLCJzdWIiOiIxMTUwNTgwODg3Mjg3NzYyMDY4NjAiLCJhenAiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJlbWFpbCI6ImR1YmVjQGd5bW10LnNrIiwiYXRfaGFzaCI6IlNJM3RBclZYMXRzbkJhbjNhRk9YMFEiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXVkIjoiNDA3NDA4NzE4MTkyLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiaGQiOiJneW1tdC5zayIsInRva2VuX2hhc2giOiJTSTN0QXJWWDF0c25CYW4zYUZPWDBRIiwidmVyaWZpZWRfZW1haWwiOnRydWUsImNpZCI6IjQwNzQwODcxODE5Mi5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImlhdCI6MTQyMTMzNTI0NiwiZXhwIjoxNDIxMzM5MTQ2fQ.KIDhDgba6ofBKGCQ0ue9w5jWgoai_QA8m9rz_0gNTWZ0BrbQH4We1NGamWYhtgiUQA-VcOPEO3zRXw8McA26bsKKG4ExZ10SS8lt5t1FOy7UvpzNHi3stpj5Z2tw1S8qCcKSIxgwt0GG5VRx9uQ2U_241B_at34TqnsT1DZYdCM";
		$fields = array(
			'id_token' => $token
		);
		$fields_string = http_build_query($fields);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

		$result = curl_exec($ch);

		curl_close($ch);

		//$result = json_decode($result);
		var_dump($result);
	}
}