<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 23.8.2014
 * Time: 14:02
 */

use Phalcon\CLI\Task;
require_once 'PHPMailer/PHPMailerAutoload.php';
require_once 'Twig/Autoloader.php';

//TODO: Logging

class NewsletterTask extends Task {

	private $mail;
	private $twig;
	private $rowCount = array();

	public function onConstruct() {
		$this->mail = new PHPMailer(true);
		$this->mail->IsSMTP();
		$this->mail->Host = $this->config->email->host;
		$this->mail->SMTPDebug = 0;
		$this->mail->SMTPAuth = true;
		$this->mail->SMTPSecure = "ssl";
		$this->mail->Port = $this->config->email->port;
		$this->mail->Username = $this->config->email->user;
		$this->mail->Password = $this->config->email->password;
		$this->mail->CharSet = "UTF-8";
		$this->mail->isHTML(true);

		Twig_Autoloader::register();
		$loader = new Twig_Loader_Filesystem(APPLICATION_PATH . '/views/');
		$this->twig = new Twig_Environment($loader, array(
			'cache' => false
		));
	}

	private function sendEmail($to, $message) {
		$this->mail->clearAllRecipients();
		$this->mail->clearAddresses();
		$this->mail->setFrom($this->config->email->user, 'GVPT Newsletter');
		$this->mail->addAddress($to);
		$this->mail->Subject = 'GVPT Newsletter ' . date("j. n. Y");

		$this->mail->msgHTML($message);

		try {
			$this->mail->Send();
		}
		catch (Exception $e) {
			echo $e->getMessage();
			return false;
		}
		return true;
	}

	private function prepareAnnouncements() {
		$sql = "SELECT id_announcement, ann_title, ann_text FROM announcement WHERE DATE(ann_created) = CURRENT_DATE()";
		try {
			$announcements = $this->db->fetchAll($sql, Phalcon\Db::FETCH_BOTH);
		}
		catch (Exception $e) {
			echo "SQL error:" . $e->getMessage() . " \n";
			exit();
		}
		if (count($announcements) > 0) {
			$this->rowCount['announcements'] = count($announcements);
			return $announcements;
		}
		else {
			$this->rowCount['announcements'] = 0;
			return null;
		}
	}

	private function prepareEvents() {
		$sql = "SELECT id_event, evt_title, evt_description, evt_startTime, evt_endTime, evt_date, evy_label, evt_place FROM event LEFT JOIN eventType ON eventType.id_eventType = event.id_eventType WHERE DATE(evt_date) >= CURRENT_DATE()";
		try {
			$events = $this->db->fetchAll($sql, Phalcon\Db::FETCH_OBJ);
		}
		catch (Exception $e) {
			echo "SQL error:" . $e->getMessage() . " \n";
			exit();
		}
		if (count($events) > 0) {
			$this->rowCount['events'] = count($events);
			return $events;
		}
		else {
			$this->rowCount['events'] = 0;
			return null;
		}

	}

	/**
	 * @param string $user User id
	 * @param string $suploMode Substitution mode
	 * @return mixed
	 */
	private function prepareSubstitution($user, $suploMode) {
		if ($suploMode != 'NONE') {
			$suploArray = array();
			if ($suploMode == 'ME') {
				$sql = "SELECT DISTINCT sup_date FROM suplo WHERE sup_date > CURRENT_DATE() AND id_user = '$user'";
			}
			else {
				$sql = "SELECT DISTINCT sup_date FROM suplo WHERE sup_date > CURRENT_DATE()";
			}
			try {
				$days = $this->db->fetchAll($sql, Phalcon\Db::FETCH_OBJ);
			}
			catch (Exception $e) {
				$this->logger->error("SQL error: " . $e->getMessage());
				echo "SQL error:" . $e->getMessage() . " \n";
				exit();
			}
			foreach ($days as $day)  {
				$day = $day->sup_date;
				if ($suploMode == 'ME') {
					$sql = "SELECT suplo.id_user, id_suplo, sup_date, sup_classes, sup_classroom, sup_subject, sup_nick, user.usr_firstName, user.usr_lastName, tmt_label, CONCAT(missing.usr_firstName, ' ', missing.usr_lastName) AS missingName FROM suplo LEFT JOIN user ON user.id_user = suplo.id_user LEFT JOIN timetable ON timetable.id_timetable = suplo.id_timetable LEFT JOIN user AS missing ON missing.usr_nick = suplo.sup_nick WHERE sup_date = '$day' AND user.id_user = '$user'";
				}
				else {
					$sql = "SELECT suplo.id_user, id_suplo, sup_date, sup_classes, sup_classroom, sup_subject, sup_nick, user.usr_firstName, user.usr_lastName, tmt_label, CONCAT(missing.usr_firstName, ' ', missing.usr_lastName) AS missingName FROM suplo LEFT JOIN user ON user.id_user = suplo.id_user LEFT JOIN timetable ON timetable.id_timetable = suplo.id_timetable LEFT JOIN user AS missing ON missing.usr_nick = suplo.sup_nick WHERE sup_date = '$day'";
				}
				try {
					$result = array();
					$result['date'] = $day;
					$result['suplo'] = $this->db->fetchAll($sql, Phalcon\Db::FETCH_OBJ);
					$suploArray[] = $result;
					$this->rowCount['suplo'] = count($result);
				}
				catch (Exception $e) {
					$this->logger->error("SQL error: " . $e->getMessage());
					echo "SQL error:" . $e->getMessage() . " \n";
					exit();
				}
			}
			return $suploArray;
		}
		else {
			return null;
		}
	}

	private function getTemplate($tags) {
		return $this->twig->render('newsletter/sendEmails.volt', $tags);
	}

	private function isEmptyMessage($announcements, $events, $suplo) {
		return ($announcements*$this->rowCount['announcements'] + $events*$this->rowCount['events']) || (($suplo != 'NONE') && $this->rowCount['suplo']);
	}

	public function sendEmailsAction() {
		$startTime = microtime(true);
		echo date("Y-m-d H:i:s") . "\n";

		$sql = "SELECT newsletter.*, usr_firstName, usr_lastName FROM newsletter LEFT JOIN user ON user.id_user = newsletter.id_user";
		try {
			$newsletter = $this->db->fetchAll($sql, Phalcon\Db::FETCH_OBJ);
		}
		catch (Exception $e) {
			echo "SQL error:" . $e->getMessage() . " \n";
			exit();
		}
		$announcements = $this->prepareAnnouncements();
		$events = $this->prepareEvents();
		foreach ($newsletter as $record) {
			echo $record->usr_firstName . " " . $record->usr_lastName . "\t\t\t\t\t\t";
			$substitution = $this->prepareSubstitution($record->id_user, $record->nws_suplo);
			$tags = array(
				'newsletter'		=> $newsletter,
				'announcements' 	=> $announcements,
				'events'			=> $events,
				'suplo'				=> $substitution,
				'applicationURI'	=> $this->config->application->infosUri
			);
			$message = $this->getTemplate($tags);
			if ($this->isEmptyMessage($record->nws_announcements, $record->nws_events, $record->nws_suplo)) {
				if ($this->sendEmail($record->nws_email, $message)) {
					echo "OK\n";
				}
				else {
					echo "ERR\n";
				}
			}
			else {
				echo "EMPTY\n";
			}

		}
		$endTime = (microtime(true) - $startTime);
		$this->logger->info("Newsletter task successfully finished in: $endTime seconds");
		echo "Done! Duration: " . $endTime . "s\n";
	}
}