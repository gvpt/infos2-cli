<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 12.8.2014
 * Time: 12:49
 */
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Logger;
use Phalcon\Events\Manager as EventManager;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;

$di->set('db', function () use ($config) {

	$eventsManager = new EventManager();

	$log = new FileLogger(APPLICATION_PATH . "/logs/db.log");

	$connection =  new DbAdapter(array(
		'host' => $config->database->host,
		'username' => $config->database->username,
		'password' => $config->database->password,
		'dbname' => $config->database->dbname,
		"options" => array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
		)
	));

	$eventsManager->attach('db', function($event, $connection) use ($log) {
		if ($event->getType() == 'beforeQuery') {
			$log->log($connection->getSQLStatement(), Logger::INFO);
		}
	});
	$connection->setEventsManager($eventsManager);

	return $connection;

});

$di->set('logger', function() use ($config) {

	$connection = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
		'host' => $config->database->host,
		'username' => $config->database->username,
		'password' => $config->database->password,
		'dbname' => $config->database->dbname,
		"options" => array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
		)
	));

	$logger = new DatabaseLogger(array(
		'db' => $connection,
		'table' => 'log'
	));

	return $logger;
});