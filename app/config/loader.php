<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 12.8.2014
 * Time: 12:49
 */

$loader = new \Phalcon\Loader();
$loader->registerDirs(
	array(
		APPLICATION_PATH . '/tasks',
		APPLICATION_PATH . '/plugins'
	)
);
$loader->register();