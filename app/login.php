<?php
/**
 * Created by PhpStorm.
 * User: Jakub
 * Date: 21.8.2014
 * Time: 18:16
 */

define('CLIENT_ID', '180622801359.apps.googleusercontent.com');
define('CLIENT_SECRET', 'vgf5wAVL8v8BE9qYoDYyfkS8');

function getActualUri() {
	$s = &$_SERVER;
	$ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
	$sp = strtolower($s['SERVER_PROTOCOL']);
	$protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
	$port = $s['SERVER_PORT'];
	$port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
	$host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
	$host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
	$uri = $protocol . '://' . $host . $s['REQUEST_URI'];
	$segments = explode('?', $uri, 2);
	$url = $segments[0];
	return $url;
}

function createLoginUrl() {
	$fields = array(
		'response_type' => 'code',
		'client_id' => CLIENT_ID,
		'redirect_uri' => getActualUri(),
		'scope' => 'profile email https://www.googleapis.com/auth/calendar',
		'state' => 'Infos2 Authorization',
		'access_type' => 'offline',
		'approval_prompt' => 'force',
		'include_granted_scopes' => 'true'

	);
	return 'https://accounts.google.com/o/oauth2/auth?' . http_build_query($fields);
}

function getTokens($code) {
	$url = 'https://accounts.google.com/o/oauth2/token';
	$fields = array(
		'code' => $code,
		'client_id' => CLIENT_ID,
		'redirect_uri' => getActualUri(),
		'client_secret' => CLIENT_SECRET,
		'grant_type' => 'authorization_code'
	);
	$fields_string = http_build_query($fields);

	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

	$result = curl_exec($ch);
	curl_close($ch);

	return json_decode($result);

}

function verifyToken($id_token) {
	$url = 'https://www.googleapis.com/oauth2/v1/tokeninfo?id_token=' . $id_token;

	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

	$result = curl_exec($ch);

	curl_close($ch);

	return json_decode($result);
}

function authenticate($credentials) {
	if (isset($credentials)) {
		$email = $credentials['email'];
	}
	$host = 'localhost';
	$dbname = 'infos3';
	$user = 'infos';
	$pass = 'infos';
	try {
		$connection = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
		/** @var $email string */
		$result = $connection->query("SELECT cli_email FROM cliUser WHERE cli_email = '$email'");
		if ($result->rowCount() > 0) {
			$result = $connection->prepare("UPDATE cliuser SET cli_accessToken = :accessToken, cli_refreshToken = :refreshToken, cli_expires = :expires, cli_issued = :issued WHERE cli_email = :email");
			$data = array(
				'accessToken' => $credentials['token']['access'],
				'refreshToken' => $credentials['token']['refresh'],
				'expires' => date("Y-m-d H:i:s", $credentials['token']['expires']),
				'issued' => date("Y-m-d H:i:s", $credentials['token']['issued']),
				'email' => $email
			);
			$result->execute($data);
			if ($result->errorCode()) {
				var_dump($result->errorInfo());
			}
			return true;
		}
		else {
			echo 'test';
			return false;
		}
	}
	catch(PDOException $e) {
		echo $e->getMessage();
		return false;
	}
}

if (isset($_GET['code'])) {
	$response = getTokens($_GET['code']);
	if (!isset($response->error)) {
		$credentials = array(
			'token' => array(
				'access'	=> $response->access_token,
				'expires'	=> time() + $response->expires_in,
				'refresh'	=> $response->refresh_token
			)
		);
		$response = verifyToken($response->id_token);
		if (!isset($response->error)) {
			$credentials['email'] = $response->email;
			$credentials['token']['issued'] = $response->issued_at;
			if (authenticate($credentials)) {
				echo 'Done!';
			}
			else {
				echo 'Chyba pri prihlasovani';
			}
		}
		else {
			echo "Google auth error " . $response->error . ":" . $response->error_description . '. DEBUGN INFO: ' . $response->debug_info;
		}

	}
	else {
		echo "Google auth error " . $response->error . ":" . $response->error_description . '. DEBUGN INFO: ' . $response->debug_info;
	}

}
else {
	header('Location: ' . createLoginUrl());
}